# Update packages
sudo apt-get update

# Install Node.js and NPM
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install npm@5.7.1
# Temporary fix for issue: https://github.com/npm/npm/issues/20605
sudo npm install --global

# Install angular
sudo npm install --unsafe-perm -g @angular/cli

# Install MongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
# Start and enable the Mongo service so that it automatically starts every time you start the machine
sudo systemctl start mongod
sudo systemctl status mongod
sudo systemctl enable mongod
